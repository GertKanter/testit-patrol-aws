#!/bin/bash
rosrun testit_dtron build_docker_image.sh
rosrun testit_patrol build_containers.sh
docker run -dt --rm -v /home/bot/tron/:/tron --name temp testit_patrol_testit:latest bash
docker exec temp /bin/bash -c "cp /tron/uppaal-tron-1.5-linux.zip /catkin_ws/src/testit_dtron/testit_dtron/tron/ && cd /catkin_ws/src/testit_dtron/testit_dtron/tron/ && unzip uppaal-tron-1.5-linux.zip"
docker commit temp testit_patrol_testit:latest
